# Prep system for Cygwin

## Download `setup-x86_64.exe`

[Download](https://www.cygwin.com/setup-x86_64.exe)

## Open `cmd.exe` and install cygwin without admin

* `Downloads\setup-x86_64.exe  --no-admin`
* Root Directory: `%userprofile%\.cygwin`
* Local Package Directory: `%userprofile%\.cygwin\dist`
* Install `wget`, `tar`, and `gawk` during install so that `apt-cyg` can be installed and ran later

## Open `Cygwin64` and install [apt-cyg](https://github.com/transcode-open/apt-cyg)

* Run the following to launch `Cygwin64`

    ```
    %userprofile%\.cygwin\bin\mintty.exe -i /Cygwin-Terminal.ico -
    ```
    
    _to use another font in mintty do something like this, replacing the font at end as needed_
    
    ```
    %userprofile%\.cygwin\bin\mintty.exe -i /Cygwin-Terminal.ico -o Font="Hack" -
    ```
    
* Execute the following in `Cygwin64`
  - **Note:** _apt-cyg can not upgrade packages...might need a different package manager, if one exists_
  - **Note:** _Unoffical apt-cyg is installed using: wget -L --no-check-certificate -O ${HOME}/apt-cyg https://raw.githubusercontent.com/kou1okada/apt-cyg/master/apt-cyg_
   
    ```
    wget -L --no-check-certificate -O ${HOME}/apt-cyg https://raw.githubusercontent.com/transcode-open/apt-cyg/master/apt-cyg
    install apt-cyg /bin
    rm -rf apt-cyg
    ```
    
# Setup Cygwin and install packages

## Install all the things you want

```
apt-cyg update
apt-cyg install curl dos2unix git nano openssh python python-devel vim chere #zsh
python -m ensurepip
pip install --upgrade pip
```
# Setup bash

## Add a task in [Cmder](http://cmder.net/)

* Task parameters
    ```
    /icon %userprofile%\.cygwin\Cygwin-Terminal.ico
    ```
* Commands
    ```
    %userprofile%\.cygwin\cygwin.bat -c "/bin/xhere /bin/bash.exe '%V'"
    ```

## Install a framework for bash
### **X** [bash-it](https://github.com/Bash-it/bash-it#installation)
_slow_
* Install
    ```
    git clone --depth=1 https://github.com/Bash-it/bash-it.git ${HOME}/.bash_it
    ${HOME}/.bash_it/install.sh
    ```

# Setup zsh

## Change the target for the `Cygwin64 Terminal` shortcut

* Before
    ```
    %userprofile%\.cygwin\bin\mintty.exe -i /Cygwin-Terminal.ico -
    ```

* After
    ```
    %userprofile%\.cygwin\bin\mintty.exe -i /Cygwin-Terminal.ico /bin/zsh --login
    ```

## Add a task in [Cmder](http://cmder.net/)

* Task parameters
    ```
    /icon "%userprofile%\.cygwin\bin\mintty.exe" /dir "%userprofile%"
    ```
* Commands
    ```
    -cur_console:d:%userprofile% -cur_console:C:C%userprofile%\.cygwin\bin\mintty.exe %userprofile%\.cygwin\bin\mintty.exe -o Transparency=0 /bin/env CHERE_INVOKING=1 /bin/zsh.exe
    ```
## Install a framework for zsh (if desired)
### [zim](https://github.com/zimfw/zimfw)
* Exit and reopen cygwin with the above commands so that zsh starts
* Type `q` to skip the zsh setup (if you do not see a prompt to configure Z shell then you are not in zsh)
* Fix path in case it broke (_it happens, not sure why_)
    ```
    export PATH=/bin:/usr/bin:/usr/local/bin:$HOME/bin:$PATH
    ```
* Install
    ```
    git clone --recursive https://github.com/zimfw/zimfw.git ${ZDOTDIR:-${HOME}}/.zim
    ```
* Prepend your zsh configuration files with the initialization templates provided by Zim
    ```
    for template_file in ${ZDOTDIR:-${HOME}}/.zim/templates/*; do
      user_file="${ZDOTDIR:-${HOME}}/.${template_file:t}"
      cat ${template_file} ${user_file}(.N) >! ${user_file}
    done
    ```
* Prepend your `${HOME}/.zshrc` with needed fixes (may not be needed if a `.zshrc` was imported)
    ```
    sed -i '1i\\n' ${HOME}/.zshrc
    sed -i '1i\export TERM="xterm-256color"' ${HOME}/.zshrc
    sed -i '1i\# Add support for 256 colors' ${HOME}/.zshrc
    sed -i '1i\\n' ${HOME}/.zshrc
    sed -i '1i\export PATH=/bin:/usr/bin:/usr/local/bin:$HOME/bin:$PATH' ${HOME}/.zshrc
    sed -i '1i\# User configuration' ${HOME}/.zshrc
    ```
* Open a new terminal and finish optimization (this is only needed once, hereafter it will happen upon desktop/tty login):
    ```
    source ${ZDOTDIR:-${HOME}}/.zlogin
    ```
* For more info on how to change prompts and install/disable modules visit the [zim git repo](https://github.com/zimfw/zimfw), and these posts on how zim works: [How-Zim-Works-pt-1](https://eriner.me/2015/12/29/How-Zim-Works-pt-1/) & [How-Zim-Works-pt-2](https://eriner.me/2016/01/06/How-Zim-Works-pt-2/)

### **X** [oh-my-zsh](https://ohmyz.sh/)
_so slow it is not usable_
* Download and run install script
    ```
    sh -c "$(wget -L --no-check-certificate https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
    ```
* Fix path issue so installed packages work in zsh under oh-my-zsh
    ```
    sed -i 's|# export PATH=$HOME/bin:/bin:$PATH|export PATH=$HOME/bin:/bin:$PATH|g' ${HOME}/.zshrc
    ```

### **X** [Prezto](https://github.com/sorin-ionescu/prezto)
_Has issues installing fonts without admin on cygwin_
* Exit and reopen cygwin with the above commands so that zsh starts
* Type `q` to skip the zsh setup (if you do not see a prompt to configure Z shell then you are not in zsh)
* Fix path incase it broke (_it happens_)
    ```
    PATH=/bin:/usr/bin:/usr/local/bin:${PATH}
    export PATH
    exec /bin/zsh
    ```
* Remove old .zshrc
    ```
    rm -f ${HOME}/.zshrc
    ```
* Clone Prezto
    ```
    git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
    ```
* Copy the Zsh configuration files provided by Prezto
    ```
    setopt EXTENDED_GLOB
    for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
      ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
    done
    ```
* Fix path issue so installed packages work in zsh with prezto
  ```
  sed -i 's|/usr/local/|/|g' ${HOME}/.zprofile
  ```
* Run the following
    ```
    sed -i '1i\' ${HOME}/.zshrc
    sed -i '1i\export TERM="xterm-256color"' ${HOME}/.zshrc
    sed -i '1i\# Add support for 256 colors' ${HOME}/.zshrc
    echo "" >> ${HOME}/.zshrc
    echo "# Load prompt theme" >> ${HOME}/.zshrc
    echo "autoload -Uz promptinit" >> ${HOME}/.zshrc
    echo "promptinit" >> ${HOME}/.zshrc
    echo "prompt paradox" >> ${HOME}/.zshrc
    echo "" >> ${HOME}/.zshrc
    echo "# Load colors for theme" >> ${HOME}/.zshrc
    echo "source ${HOME}/.colors" >> ${HOME}/.zshrc
    ```
* Select powerlevel9k as the theme (makes prompt slow)
    ```
    sed -i "s|zstyle ':prezto:module:prompt' theme 'sorin'|zstyle ':prezto:module:prompt' theme 'powerlevel9k'|g" ${HOME}/.zpreztorc
    ```
* Select paradox as the theme and save (skwp OR zefram may be prefered)
    ```
    # prompt -s paradox # skwp zefram
    ```
* install powerline fonts
> Not working
    ```
    pip install powerline-status
    ```
    > OR
    ```
    git clone https://github.com/powerline/fonts.git --depth=1
    cd fonts
    ./install.sh
    <!-- wget https://raw.githubusercontent.com/powerline/fonts/master/fontconfig/50-enable-terminess-powerline.conf -->
    mkdir -p ${HOME}/.config/fontconfig/
    wget --no-check-certificate -O ${HOME}/.config/fontconfig/conf.d https://raw.githubusercontent.com/powerline/fonts/master/fontconfig/50-enable-terminess-powerline.conf
    cd ..
    rm -rf fonts
    ```


### **X** [antigen](https://github.com/zsh-users/antigen)
_Has weird issues where antigen dosent load when prompt starts_ (**It may be worth trying again with a new release**)
* Exit and reopen cygwin with the above commands so that zsh starts
* Type `q` to skip the zsh setup (if you do not see a prompt to configure Z shell then you are not in zsh)
* Fix path in case it broke (_it happens_)
    ```
    export PATH=/bin:/usr/bin:/usr/local/bin:$HOME/bin:$PATH
    ```
* Install antigen
    ```
    mkdir -p ${HOME}/.antigen
    wget -L --no-check-certificate -O ${HOME}/.antigen/antigen.zsh git.io/antigen
    ```
