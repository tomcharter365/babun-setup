# Tell Windows where to find the Go binaries (optional)
**Note:** Without following the steps below, only babun/cygwin will be able to use Go.
_This allows all other Windows programs such as an IDE like Visual Studio Code to use Go as well_
> [For additional documentation you can visit this link](http://www.wadewegner.com/2014/12/easy-go-programming-setup-for-windows/)

1. Press `Start Button` (or click the `Windows Key` on the keyboard)
1. Type `path`
1. Select `Edit environment variables for your account`
1. Select the `Path` variable and click the `Edit...` button
1. Append the path of `$GOBIN`. (If left as default the `$GOBIN` path is `C:\Go\bin`.)
    * eg: `%USERPROFILE%\.babun;C:\Go\bin`
1. Click `OK` to save
1. Click `New...`
1. For `Variable name` enter `GOPATH`
1. For `Variable value` enter `%USERPROFILE%\Documents\go` (This is the default `$GOPATH`. If it was changed from default use that path instead.)
1. Click `OK` to save
1. Click `OK` to save and exit

## You may also be interested in...
* [Git_binaries_variables](https://gitlab.com/tomcharter365/babun-setup/blob/master/notes/Git_binaries_variables.md)