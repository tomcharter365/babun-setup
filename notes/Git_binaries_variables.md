# Tell Windows where to find the Git binaries (optional)
**Note:** Without following the steps below, only babun/cygwin will be able to use Git.
_This allows all other Windows programs such as an IDE like Visual Studio Code to use Git as well_
> [For additional documentation you can visit this link](http://www.wadewegner.com/2014/12/easy-go-programming-setup-for-windows/)

1. Press `Start Button` (or click the `Windows Key` on the keyboard)
1. Type `path`
1. Select `Edit environment variables for your account`
1. Select the `Path` variable and click the `Edit...` button
1. Append the path of the Git binaries. (If left as default the path of the Git binaries are `C:\Program Files\Git\bin`.)
    * eg: `%USERPROFILE%\.babun;C:\Program Files\Git\bin`
1. Click `OK` to save and exit

## You may also be interested in...
* [Go_binaries_variables](https://gitlab.com/tomcharter365/babun-setup/blob/master/notes/Go_binaries_variables.md)