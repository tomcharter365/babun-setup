#!/bin/bash

# Variables:
# ----------

arch="$(wmic OS get OSArchitecture /value | sed '/^[[:space:]]*$/d' | grep -Eo '[^=]*$' | cut -d- -f 1)"
tmpdir="/home/${USER}/go-tmp"

# Functions:
# ----------

ask_if_rdy() {
    echo
    read -r -p "Did you add the environmental variables & aliases into your shell profile(s) / rc file(s)? [Y/n]: " response
    echo
    if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
      true
    elif [[ ! "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
      ask_if_rdy
    fi
}

# Script:
# ----------

### Install the latest stable version of go for windows via zip file
mkdir "${tmpdir}"
cd "${tmpdir}" || exit

# Adjust $arch to march url schema
if [[ "${arch}" =~ 32 ]] ; then
  arch=386
elif [[ "${arch}" =~ 64 ]] ; then
  arch=amd64
fi

# Discover link and download
latest_go_release="$(curl -L https://storage.googleapis.com/golang/ | sed -e $'s|>|>\\\n|g' | grep -iv "beta\|rc.\|sha256\|asc\|msi" | grep -i "windows" | grep -i "${arch}" | cut -d "<" -f 1 | sort -t. -k 2,4nr | head -1)"
prefix="https://storage.googleapis.com/golang/"
url=${prefix}${latest_go_release}
wget "${url}"

# Make dirs needed for Go without admin
mkdir -p /cygdrive/c/Users/"${USER}"/Documents/go/tmp
mkdir -p /cygdrive/c/Users/"${USER}"/Documents/go/bin
mkdir -p /cygdrive/c/Users/"${USER}"/Documents/go/pkg
mkdir -p /cygdrive/c/Users/"${USER}"/Documents/go/src
unzip -u "${latest_go_release}" -d /cygdrive/c/

# Rename 'go' to 'Go' in order to match offical documentation
mv /cygdrive/c/go /cygdrive/c/Go

### Ask to add Environmental Variables and Aliases to shell profile / rc file
printf "\n  \e[0;35madd the following into your shell profile(s) / rc file(s):\e[0m\n"
printf "    # go environment\n"
printf "      export GOROOT='C:\\\\\\\\Go'\n"
printf "      export GOBIN=\${GOROOT}'\\\\\\\\bin'\n"
printf "      export PATH=\${PATH}:\${GOBIN}\n"
printf "      export GOPATH='C:\\\\\\\\Users\\\\\\\\'\${USER}'\\\\\\\\Documents\\\\\\\\go'\n"
printf "      export TMPDIR=\${GOPATH}'\\\\\\\\tmp'\n"
printf "    # go get fix for newer git versions\n"
printf "      export PATH=/home/\${USER}/bin:\${PATH}\n"
printf "    # go alias\n"
printf "      alias go=\${GOBIN}/go.exe\n"

printf "\n  \e[0;35mYou may want to add the Go binaries to Windows if you want anyother programs to use Go, if so visit this page:\e[0m\n"
printf "      https://gitlab.com/tomcharter365/babun-setup/blob/master/notes/Go_binaries_variables.md\n"

ask_if_rdy
printf "\n  \e[0;34mPlease start a new shell session and then run 'go-for-cygwin_fixes-and-validate.sh' so we can validate that go was successfully installed.\e[0m\n\n"

### Cleanup
cd /home/"${USER}" || exit
rm -rf "${tmpdir}"
