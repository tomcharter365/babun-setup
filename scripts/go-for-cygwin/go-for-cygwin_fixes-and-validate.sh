#!/bin/bash

## Notes
# Proved dificult to use the go alias found in '~/.bash_profile'
  # `alias go=/cygdrive/c/Go/bin/go.exe`
# Decided to use full path of 'go.exe' insted which resolves issues.
go='/cygdrive/c/Go/bin/go.exe'

### Fix 'go get' in cygwin/babun with updated git versions [gist of fix](https://gist.github.com/thekid/83b8ac613dba829d0c6cacdc33e50cdb) and [issue posted](https://github.com/golang/go/issues/23155). [An archive of git.go can be found here](https://gitlab.com/tomcharter365/babun-setup/blob/master/scripts/go-for-cygwin/archive.git.go).
cd /home/"${USER}"/bin/ || exit
wget --no-check-certificate -O git.go https://gist.githubusercontent.com/thekid/83b8ac613dba829d0c6cacdc33e50cdb/raw/e615d737672a80b88b23d19be0655433d7d3f019/git.go
go build -o git.exe git.go
export PATH=/home/"${USER}"/bin:${PATH}
rm -rf git.go
cd /home/"${USER}" || exit

## Validate that Go has successfully installed
${go} get github.com/golang/example/hello
cd "${GOPATH}"/src/github.com/golang/example/hello/ || exit
errlog=()

# test go get
if [ -f ./hello.go ] ; then
  true
else
  IFS=$'\n' errlog+=("$(printf "• 'go get' did not work properly.")")
fi

# test go run
if [[ $(${go} run ./hello.go) =~ "examples" ]] ; then
  ${go} build
else
  IFS=$'\n' errlog+=("$(printf "• 'go run' did not work properly.")")
fi

# test go build
if [ -f ./hello.exe ] ; then
  true
else
  IFS=$'\n' errlog+=("$(printf "• 'go build' did not work properly.")")
fi

# test exe
if [[ $(./hello.exe) =~ "examples" ]] ; then
  true
else
  IFS=$'\n' errlog+=("$(printf "• Test of compiled exe did not work properly.")")
fi

# Cleanup
rm -rf "${GOPATH}"/src/github.com/golang/example

# test results
if [ "${#errlog[@]}" -ge 1 ] ; then
  printf "\n  \e[0;31mGo did not install properly.\e[0m\n\n"
  printf "  The following errors were encountered during validation:\e[0m\n"
  for (( i=0; i<${#errlog[@]}; i++ )) ; do
    printf "%s    \e[0;33m${errlog[$i]}\e[0m\n"
  done
  printf "\n  \e[0;35mDelete Go from your system and try again\e[0m\n"
  printf '    • Delete C:\\Go'"\n"
  printf '%s    • Delete C:\\Users\\'"${USER}"'\\Documents\\go'"\n"
  printf '    • Remove environmental variables and aliases from shell profile'"\n"
  echo
else
  printf "%s\n  \e[0;32mGo $(${go} version | cut -d" " -f 3,4 | sed "s|go||g; s|windows/|for windows |g; s|386|32-bit|; s|amd64|64-bit|") has successfully installed.\e[0m\n"
  printf '    • Your GOROOT is C:\\Go'"\n"
  printf '%s    • Your GOPATH is C:\\Users\\'"${USER}"'\\Documents\\go'"\n"
  echo
fi
