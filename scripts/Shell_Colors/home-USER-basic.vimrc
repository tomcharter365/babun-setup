" /home/${USER}/.vimrc
"""""""""""""""""""""""""""""""""""""

" Vim Basic
set runtimepath+=~/.vim_runtime
source ~/.vim_runtime/vimrcs/basic.vim

" golang
let g:go_version_warning = 0

" shortcuts
set pastetoggle=<F2>
