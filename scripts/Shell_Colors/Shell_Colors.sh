#!/bin/bash

# =====================================
# Variables
# =====================================

## Colors
blue='\e[0;34m'         # blue
green='\e[0;32m'        # green
yellow='\e[0;33m'       # yellow
purple='\e[0;35m'       # purple
no_color='\e[0m'        # Text Reset

## Questions
nano1="$(printf "%s  ${purple}Do you want to install scopatzs nanorc, which will provide syntax highlighting for nano?${no_color}\n")
$(printf "%s\n    ${blue}For more info on scopatzs nanorc, go here: https://github.com/scopatz/nanorc ${no_color}\n\n")
$(printf "\n  [Y/n]: ")"
nano2="$(printf "%s  ${purple}Will you use a dark background color for your shell?${no_color} [Y/n]: \n")"
tomnano1="$(printf "%s  ${purple}Would you like to prepend the current '~/.nanorc' with Tom's user settings for nano?${no_color}\n")
$(printf "%s\n    ${blue}For more info on Tom's nanorc, go here: ${home_tom_nanorc} ${no_color}\n\n")
$(printf "\n  [Y/n]: ")"
vim1="$(printf "%s  ${purple}Do you want to install Ultimate vimrc, which will provide syntax highlighting for vim?${no_color}\n")
$(printf "%s\n    ${blue}For more info on Ultimate vimrc, go here: https://github.com/amix/vimrc ${no_color}\n\n")
$(printf "\n  [Y/n]: ")"
vim2="$(printf "%s  ${purple}Are you currently using babun?${no_color} [Y/n]: \n")"
vim3="$(printf "%s  ${purple}Do you want to install the full Awesome version?${no_color} [Y/n]: \n")"
tomvim1="$(printf "%s  ${purple}Would you like to overwrite the current '~/.vimrc' with Tom's Ultimate vimrc?${no_color}\n")
$(printf "%s\n    ${blue}For more info on Tom's Ultimate vimrc, go here: ${home_tom_vimrc} ${no_color}\n\n")
$(printf "\n  [Y/n]: ")"
tomvim2="$(printf "%s  ${purple}Do you want to use Toms extras and debloat script for Tom's Ultimate vimrc?${no_color} [Y/n]: \n")"

## URLs
home_tom_nanorc='https://gitlab.com/tomcharter365/babun-setup/raw/master/scripts/Shell_Colors/home-tom-.nanorc'
etc_vimrc='https://gitlab.com/tomcharter365/babun-setup/raw/master/scripts/Shell_Colors/etc-vimrc'
home_USER_awesome_vimrc='https://gitlab.com/tomcharter365/babun-setup/raw/master/scripts/Shell_Colors/home-USER-awesome.vimrc'
home_tom_vimrc='https://gitlab.com/tomcharter365/babun-setup/raw/master/scripts/Shell_Colors/home-tom-.vimrc'
extras_and_debloat='https://gitlab.com/tomcharter365/babun-setup/raw/master/scripts/Shell_Colors/Toms-Vim-Awesome-extras-and-debloat.sh'

# =====================================
# Functions
# =====================================

ask_q_tomnano1() {
  echo
  read -r -p "${tomnano1}" response
  echo
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
    # answered yes
    wget --no-check-certificate -O /home/"${USER}"/shell-color-tmp/home-tom-.nanorc ${home_tom_nanorc}
    cat /home/"${USER}"/shell-color-tmp/home-tom-.nanorc /home/"${USER}"/.nanorc > /home/"${USER}"/shell-color-tmp/nano_tmp; mv /home/"${USER}"/shell-color-tmp/nano_tmp /home/"${USER}"/.nanorc
  elif [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    # answered no
    printf "%s\n  ${yellow}Skipping install of Tom's user settings for nano${no_color}\n\n"
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
    # unacceptable answer, ask again
    ask_q_tomnano1
  fi
}

ask_q_nano2() {
  echo
  read -r -p "${nano2}" response
  echo
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
    # answered yes
    # optional color changes for nano; needed if using a dark background
    for i in /home/"${USER}"/.nano/*.nanorc; do sed -i "s|brightblack|magenta|g" "${i}" ; done
    for i in /home/"${USER}"/.nano/*.nanorc; do sed -i "s|black|magenta|g" "${i}" ; done
    for i in /home/"${USER}"/.nano/*.nanorc; do sed -i "s|brightwhite,cyan|red,yellow|g" "${i}" ; done
    printf "%s  ${yellow}Changed syntax highlighting to go well on top of a dark background${no_color}\n"
    ask_q_tomnano1
  elif [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    # answered no
    ask_q_tomnano1
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
    # unacceptable answer, ask again
    ask_q_nano2
  fi
}

ask_q_nano1() {
  echo
  read -r -p "${nano1}" response
  echo
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
    # answered yes
    git clone https://github.com/scopatz/nanorc.git
    mkdir -p /home/"${USER}"/.nano/
    cp -r nanorc/* /home/"${USER}"/.nano/
    cat /home/"${USER}"/.nano/nanorc >> /home/"${USER}"/.nanorc
    # Temp fix to workaround https://github.com/scopatz/nanorc/issues/191
    rm -rf /home/"${USER}"/.nano/yaml.nanorc
    wget --no-check-certificate -O /home/"${USER}"/.nano/yaml.nanorc https://raw.githubusercontent.com/serialhex/nano-highlight/master/yaml.nanorc
    ask_q_nano2
  elif [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    # answered no
    printf "%s\n  ${yellow}Skipping install of scopatz's nanorc${no_color}\n\n"
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
    # unacceptable answer, ask again
    ask_q_nano1
  fi
}

ask_q_vim3() {
  echo
  read -r -p "${vim3}" response
  echo
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
    # answered yes
    sh ~/.vim_runtime/install_awesome_vimrc.sh    # Awesome version
    wget --no-check-certificate -O - ${home_USER_awesome_vimrc} >> /home/"${USER}"/.vimrc
  elif [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    # answered no
    sh ~/.vim_runtime/install_basic_vimrc.sh      # Basic version
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
    # unacceptable answer, ask again
    ask_q_vim3
  fi
}

ask_q_vim2() {
  echo
  read -r -p "${vim2}" response
  echo
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
    # answered yes
    ## Overwrite current `/etc/vimrc` with Ultimate vimrc patch for babun
    wget --no-check-certificate -O - ${etc_vimrc} > /etc/vimrc
    ask_q_vim3
  elif [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    # answered no
    printf "%s\n  ${yellow}Not using babun${no_color}\n"
    ask_q_vim3
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
    # unacceptable answer, ask again
    ask_q_vim2
  fi
}

ask_q_vim1() {
  echo
  read -r -p "${vim1}" response
  echo
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
    # answered yes
    # Downlaod Ultimate vimrc by amix
    git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
    ask_q_vim2
  elif [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    # answered no
    printf "%s\n  ${yellow}Skipping install of Ultimate vimrc${no_color}\n"
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
    # unacceptable answer, ask again
    ask_q_vim1
  fi
}

ask_q_tomvim2() {
  echo
  read -r -p "${tomvim2}" response
  echo
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
    # answered yes
    wget --no-check-certificate -O - ${extras_and_debloat} > /home/"${USER}"/Vim-Awesome-extras-and-debloat.sh
    bash /home/"${USER}"/Vim-Awesome-extras-and-debloat.sh
    find /home/"${USER}"/Vim-Awesome-extras-and-debloat.sh -delete
  elif [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    # answered no
    printf "%s\n  ${yellow}Will not run Vim-Awesome-extras-and-debloat.sh${no_color}\n"
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
    # unacceptable answer, ask again
    ask_q_tomvim2
  fi
}

ask_q_tomvim1() {
  echo
  read -r -p "${tomvim1}" response
  echo
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
    # answered yes
    ## Overwrite current `~/.vimrc` with Tom's Ultimate vimrc
    wget --no-check-certificate -O - ${home_tom_vimrc} > /home/"${USER}"/.vimrc
    ask_q_tomvim2
  elif [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    # answered no
    printf "%s\n  ${yellow}Will not overwrite current '~/.vimrc'${no_color}\n"
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
    # unacceptable answer, ask again
    ask_q_tomvim1
  fi
}

# =====================================
# script
# =====================================
mkdir /home/"${USER}"/shell-color-tmp
cd /home/"${USER}"/shell-color-tmp || exit

## [nanorc](https://github.com/nanorc/nanorc) is gone, using [scopatz's nanorc](https://github.com/scopatz/nanorc) instead
ask_q_nano1

## [Ultimate vimrc](https://github.com/amix/vimrc)
ask_q_vim1

## [Tom's Ultimate vimrc](${home_tom_vimrc})
ask_q_tomvim1

## Cleanup
rm -rf /home/"${USER}"/shell-color-tmp

## Announce success
printf "%s\n  ${green}Shell_Colors.sh has finished.${no_color}\n\n"
