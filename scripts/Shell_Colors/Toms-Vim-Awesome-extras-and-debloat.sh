#!/bin/bash

## =====================================
## Tom's personal Vim Awesome extra settings & debloat
## =====================================

# install Vundel
git clone https://github.com/VundleVim/Vundle.vim.git /home/${USER}/.vim/bundle/Vundle.vim

# install Vundel plugins from terminal
vim +PluginInstall +qall

## Ultimate vimrc plugins - marked for removal (sources_non_forked)
# Plugin 'jlanzarotta/bufexplorer'          " BufExplorer Plugin for Vim
# Plugin 'ctrlpvim/ctrlp.vim'               " Ctrlp for file navigation
# Plugin 'junegunn/goyo.vim'                " Distraction-free writing in Vim
# Plugin 'amix/open_file_under_cursor.vim'  " Open file under cursor when pressing gf (if the text under the cursor is a path)
# Plugin 'wting/rust.vim'                   " Vim support for Rust file detection and syntax highlighting
# Plugin 'scrooloose/snipmate-snippets'     " A collection of snippets for snipmate
# Plugin 'nvie/vim-flake8'                  " Flake8 plugin for Vim
# Plugin 'michaeljsmith/vim-indent-object'  " Vim plugin that defines a new text object representing lines of code at the same indent level
# Plugin 'digitaltoad/vim-pug'              " Vim Pug (formerly Jade) template engine syntax highlighting and indention
# Plugin 'garbas/vim-snipmate'              " snipMate.vim aims to be a concise vim script that implements some of TextMate's snippets features in Vim
# Plugin 'honza/vim-snippets'               " vim-snipmate default snippets (Previously snipmate-snippets)

# Declare array with all plugins marked for removal
unwanted_plugins=(
bufexplorer
ctrlp.vim
goyo.vim
# nginx.vim
open_file_under_cursor.vim
rust.vim
snipmate-snippets
vim-flake8
vim-indent-object
vim-pug
vim-snipmate
vim-snippets
# vim-zenroom2
)

# Remove unwanted plugings which were installed by Ultimate vimrc
cd /home/${USER}/.vim_runtime/sources_non_forked/
for i in "${unwanted_plugins[@]}"; do git submodule deinit -f "${i}" && git rm -rf "${i}"; done

# remove lines from `vim-go/templates/hello_world.go` as every new file will have this text
mv /home/${USER}/.vim_runtime/sources_non_forked/vim-go/templates/hello_world.go /home/${USER}/.vim_runtime/sources_non_forked/vim-go/templates/hello_world.old.go
touch /home/${USER}/.vim_runtime/sources_non_forked/vim-go/templates/hello_world.go
