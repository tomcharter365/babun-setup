#!/bin/bash

# Variables:
# ----------

# Colors:
  cyan='\e[0;36m'         # cyan
  green='\e[0;32m'        # green
  purple='\e[0;35m'       # purple
  no_color='\e[0m\n'      # Text Reset

# Functions:
# ----------

version() {
  echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'
}

ask_for_path() {
  printf "%s\n""${purple}" && read -r -p "   Atom will be installed to ${atom_path}atom-x64-windows, is this OK? [Y/n]: " response  && printf "%s${no_color}"
  if [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    printf "%s\n""${cyan}" && read -r -p "   Please type the desired install path for Atom using the cygwin path format (For example: /cygdrive/c/Users/${USER}/): " atom_path  && printf "%s${no_color}"
    ask_for_path
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
  ask_for_path
  fi
}

# Script:
# ----------

printf "%s\n  ${cyan}Checking Github for the latest release of Atom x64 portable for Windows. Please wait...${no_color}\n"
latest_atom_release_url="$(curl -s -L https://github.com/atom/atom/releases/latest | grep -i 'atom-x64-windows.zip' | grep -i '<a href=' | cut -d "\"" -f 2)"
latest_atom_version="$(echo "${latest_atom_release_url}" | cut -d "/" -f 6 | sed "s|v||g")"
printf "%s\n  ${cyan}Checking if Atom x64 portable for Windows is installed on this PC. Please wait...${no_color}\n"
atom_path="$(find /cygdrive/c -type d -iname "Atom x64" -printf '%h' -quit 2>/dev/null)"

if [ -n "$atom_path" ]; then
  printf "%s\n  ${cyan}Atom x64 portable for Windows is installed, checking if update is needed${no_color}\n"
  current_atom_version="$(grep "Package: atom@" "${atom_path}"/Atom\ x64/resources/LICENSE.md | cut -d "@" -f 2)"
  if [ "$(version "${latest_atom_version}")" -le "$(version "${current_atom_version}")" ]; then
    printf "%s\n  ${green}No update needed, Atom is already on the latest release (${latest_atom_version}).${no_color}\n"
  else
    printf "%s\n  ${cyan}Atom x64 portable for Windows will now be updated to the latest release (${latest_atom_version}).${no_color}\n"
    cd "${atom_path}"/.. || exit
    prefix_url="https://github.com"
    url="$prefix_url""$latest_atom_release_url"
    wget "${url}" -O ./atom-x64-windows.zip
    unzip -uo atom-x64-windows.zip -d ./atom-x64-windows/
    rm -rf atom-x64-windows.zip
    printf "%s\n  ${green}Atom x64 portable for Windows has been updated to the latest release (${latest_atom_version}) inside ${atom_path}.${no_color}\n"
  fi
else
  atom_path=/cygdrive/c/Users/${USER}/AppData/Roaming/Portable\ Apps/VIA-Portable-Apps/ # change path as desired.
  ask_for_path
  printf "%s\n  ${cyan}Atom x64 portable for Windows will now be installed using the latest release (${latest_atom_version}).${no_color}\n"
  cd "${atom_path}" || exit
  prefix_url="https://github.com"
  url="$prefix_url""$latest_atom_release_url"
  wget "${url}" -O ./atom-x64-windows.zip
  unzip -uo atom-x64-windows.zip -d ./atom-x64-windows/
  rm -rf atom-x64-windows.zip
  printf "%s\n  ${green}Atom x64 portable for Windows has been installed to ${atom_path} and is on the latest release (${latest_atom_version}).${no_color}\n"
fi
