# Path to your oh-my-zsh installation.
  export ZSH=${HOME}/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
  #ZSH_THEME="re5et"
  ZSH_THEME="babun"

# Uncomment the following line to use case-sensitive completion.
  # CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
  HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
  # DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
  export UPDATE_ZSH_DAYS=1

# Uncomment the following line to disable colors in ls.
  # DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
  # DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
  # ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
  # COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
  # DISABLE_UNTRACKED_FILES_DIRTY="true"

# History - Logging
  # HISTFILE=~/.zsh_history_$(date '+%Y%m%d_%H_%M_%S_%N').txt

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
  HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
  # ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
  plugins=(git)

# Fix the `Insecure completion-dependent directories detected` errors
  ZSH_DISABLE_COMPFIX=true

# User configuration
  export PATH=${HOME}/bin:/usr/local/bin:${PATH}
  # export MANPATH="/usr/local/man:$MANPATH"

# Only source 'oh-my-zsh.sh' if using zsh
if [[ "$(ps -ef | awk '$2==pid' pid=$$ | awk -F/ '{print $NF}')" == zsh ]] ; then
  source ${ZSH}/oh-my-zsh.sh
else
  true
fi

# You may need to manually set your language environment
  # export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
  # if [[ -n $SSH_CONNECTION ]]; then
  #   export EDITOR='vim'
  # else
  #   export EDITOR='mvim'
  # fi

# Compilation flags
  # export ARCHFLAGS="-arch x86_64"

# ssh
  # export SSH_KEY_PATH="~/.ssh/dsa_id"

# Color Exports:
  ## Reset Color:
    export Color_Off='\e[0m'       # Text Reset
  ## Regular Colors:
    export Black='\e[0;30m'        # Black
    export Red='\e[0;31m'          # Red
    export Green='\e[0;32m'        # Green
    export Yellow='\e[0;33m'       # Yellow
    export Blue='\e[0;34m'         # Blue
    export Purple='\e[0;35m'       # Purple
    export Cyan='\e[0;36m'         # Cyan
    export White='\e[0;37m'        # White
  ## Bold Colors:
    export BBlack='\e[1;30m'       # Black
    export BRed='\e[1;31m'         # Red
    export BGreen='\e[1;32m'       # Green
    export BYellow='\e[1;33m'      # Yellow
    export BBlue='\e[1;34m'        # Blue
    export BPurple='\e[1;35m'      # Purple
    export BCyan='\e[1;36m'        # Cyan
    export BWhite='\e[1;37m'       # White
  ## Underlined Colors:
    export UBlack='\e[4;30m'       # Black
    export URed='\e[4;31m'         # Red
    export UGreen='\e[4;32m'       # Green
    export UYellow='\e[4;33m'      # Yellow
    export UBlue='\e[4;34m'        # Blue
    export UPurple='\e[4;35m'      # Purple
    export UCyan='\e[4;36m'        # Cyan
    export UWhite='\e[4;37m'       # White
  ## Background Colors:
    export On_Black='\e[40m'       # Black
    export On_Red='\e[41m'         # Red
    export On_Green='\e[42m'       # Green
    export On_Yellow='\e[43m'      # Yellow
    export On_Blue='\e[44m'        # Blue
    export On_Purple='\e[45m'      # Purple
    export On_Cyan='\e[46m'        # Cyan
    export On_White='\e[47m'       # White
  ## High Intensity Colors:
    export IBlack='\e[0;90m'       # Black
    export IRed='\e[0;91m'         # Red
    export IGreen='\e[0;92m'       # Green
    export IYellow='\e[0;93m'      # Yellow
    export IBlue='\e[0;94m'        # Blue
    export IPurple='\e[0;95m'      # Purple
    export ICyan='\e[0;96m'        # Cyan
    export IWhite='\e[0;97m'       # White
  ## Bold High Intensity Colors:
    export BIBlack='\e[1;90m'      # Black
    export BIRed='\e[1;91m'        # Red
    export BIGreen='\e[1;92m'      # Green
    export BIYellow='\e[1;93m'     # Yellow
    export BIBlue='\e[1;94m'       # Blue
    export BIPurple='\e[1;95m'     # Purple
    export BICyan='\e[1;96m'       # Cyan
    export BIWhite='\e[1;97m'      # White
  ## High Intensity background Colors:
    export On_IBlack='\e[0;100m'   # Black
    export On_IRed='\e[0;101m'     # Red
    export On_IGreen='\e[0;102m'   # Green
    export On_IYellow='\e[0;103m'  # Yellow
    export On_IBlue='\e[0;104m'    # Blue
    export On_IPurple='\e[0;105m'  # Purple
    export On_ICyan='\e[0;106m'    # Cyan
    export On_IWhite='\e[0;107m'   # White

# Path Exports
  ## Linux Path Exports
    export l_c_path=/cygdrive/c
    export l_win_user_path="${l_c_path}/Users/${USER}"
    export l_programs_path="${l_c_path}/Program\ Files"
    export l_programs_x86_path="${l_c_path}/Program\ Files\ (x86)"
    export l_desktop_path="${l_win_user_path}/Desktop"
    export l_documents_path="${l_win_user_path}/Documents"
    export l_scripts_path="${l_documents_path}/Scripts"
    export l_linux_scripts_path="${l_scripts_path}/Linux"
    export l_windows_scripts_path="${l_scripts_path}/Windows"
    export l_testing_scripts_path="${l_scripts_path}/Testing"
    export l_roaming_path="${l_win_user_path}/AppData/Roaming"
    export l_sm_programs_path="${l_roaming_path}/Microsoft/Windows/Start\ Menu/Programs"
    export l_pr_path="${l_roaming_path}/Portable\ Apps/Portable\ Repo"
    export l_vpr_path="${l_roaming_path}/Portable\ Apps/VIA-Portable-Apps"
    export l_linux_user_path="/home/${USER}"
    export l_full_linux_user_path="${l_win_user_path}/.babun/cygwin/home/${USER}"
    export l_logs_path="${l_win_user_path}/tbuico-documents/Logs"
  ## Linux Program Path Exports
    export l_cmder_path="${l_vpr_path}/Portable\ Apps/VIA-Portable-Apps/cmder_mini/vendor/conemu-maximus5/ConEmu64.exe"
    export l_chrome_path="${l_programs_x86_path}/Google/Chrome/Application/chrome.exe"
  ## Windows Path Exports
    # export w_c_path="C:"
    export w_user_path='C:\\Users\\'${USER}
    export w_babun_root_path=${w_user_path}'\\.babun\\cygwin'
    export w_babun_user_path=${w_babun_root_path}'\\home\\'${USER}
    export w_programs_path='C:\\Program\ Files'
    export w_programs_x86_path='C:\\Program\ Files\ \(x86\)'
    export w_desktop_path=${w_user_path}'\\Desktop'
    export w_documents_path=${w_user_path}'\\Documents'
    export w_scripts_path=${w_documents_path}'\\Scripts'
    export w_linux_scripts_path=${w_scripts_path}'\\Linux'
    export w_windows_scripts_path=${w_scripts_path}'\\Windows'
    export w_testing_scripts_path=${w_scripts_path}'\\Testing'
    export w_roaming_path=${w_user_path}'\\AppData\\Roaming'
    export w_sm_programs_path=${w_roaming_path}'\\Microsoft\\Windows\\Start\ Menu\\Programs'
    export w_pr_path=${w_roaming_path}'\\Portable\ Apps\\Portable\ Repo'
    export w_vpr_path=${w_roaming_path}'\\Portable\ Apps\\VIA-Portable-Apps'
    export w_logs_path=${w_user_path}'\\tbuico-documents\\Logs'
  ## Package Paths
    ### ansible
      ANSIBLE=/opt/ansible
      export PATH=${PATH}:${ANSIBLE}/bin
      export PYTHONPATH=${ANSIBLE}/lib
      export ANSIBLE_LIBRARY=${ANSIBLE}/library
    ### java
      export JAVA_HOME=${l_programs_path}/Java
      export PATH=${JAVA_HOME}/jdk1.8.0_51/bin:${PATH}
      export PATH=${JAVA_HOME}/jre1.8.0_51/bin:${PATH}
    ### golang
      export GOROOT='C:\\Go'
      export GOBIN=${GOROOT}'\\bin'
      export PATH=${PATH}:${GOBIN}
      export GOPATH='C:\\Users\\'${USER}'\\Documents\\go'
      export TMPDIR=${GOPATH}'\\tmp'
    ### ruby
      export RUBYROOT='C:\\Ruby25-x64'
      export RUBYBIN=${RUBYROOT}'\\bin'
      export PATH=${PATH}:${RUBYBIN}

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# Example aliases
  # alias zshconfig="mate ~/.zshrc"
  # alias ohmyzsh="mate ~/.oh-my-zsh"

# git Aliases
  alias git-run="bash ${l_linux_scripts_path}/git-run_win.sh"   # Script the syncs major git repos automatically

# CD Aliases
  alias cd-user="cd ${l_win_user_path}"
  alias cd-sm="cd ${l_sm_programs_path}"
  alias cd-pr="cd ${l_pr_path}"
  alias cd-vpr="cd ${l_vpr_path}"

# Windows Aliases
  alias ping="ping -t"  # Make windows ping act more like linux ping by default
  alias empty-bin=${l_windows_scripts_path}/empty_recycle-bin.bat
  alias update-cygwin=${l_windows_scripts_path}/.babun/update.bat

# Misc Package Aliases
  alias ssh="ssh -o TCPKeepAlive=yes -o ServerAliveInterval=120"   # Keep ssh session alive
  alias vi="vim"                                                   # use vim, not vi

# Shell Change Aliases (needs to be in both `~/.bashrc` & `~/.zshrc`)
  # alias cs-bash="exec bash"  # change shell to bash
  # alias cs-zsh="exec zsh"    # change shell to zsh

# Z shell Speciffic Aliases
  alias ll="l"

# Golang Aliases
  alias go=/cygdrive/c/Go/bin/go.exe

# Java Aliases
  alias java="/cygdrive/c/Program\ Files/Java/jdk1.8.0_51/bin/java.exe"
  alias jar="/cygdrive/c/Program\ Files/Java/jdk1.8.0_51/bin/jar.exe"

# Python Aliases
  # alias python=python3
  # alias pip=pip3

# Ruby Aliases
  alias gem="$(cygpath "${RUBYBIN}")/gem.cmd"
  alias rubydk="$(cygpath "${RUBYBIN}")/ridk.cmd"

# intersting example
# alias addone='{ num=$(cat -); echo "input: $num"; echo "result:$(($num+1))"; }<<<' # Use like: `addone 10`

# git plugins
  # [git-subrepo](https://github.com/ingydotnet/git-subrepo)
    # Install git-subrepo
      # wget --no-check-certificate https://github.com/ingydotnet/git-subrepo/archive/0.3.1.zip -O git-subrepo_0.3.1.zip && unzip -uo git-subrepo_0.3.1.zip -d ${HOME}/.git-subrepo
      ## git clone https://github.com/ingydotnet/git-subrepo ${HOME}/.git-subrepo
    # Check for updates (release 0.3.1 is curently the only working for babun)
      # subrepo_upgrade() {
      #   if [[ "$(ps -ef | awk '$2==pid' pid=$$ | awk -F/ '{print $NF}')" == zsh ]] ; then   # robust check if shell is zsh as `read` syntax changes
      #     echo
      #     read "response?[git-subrepo upgrade] Do you want to upgrade git-subrepo? [Y/n]: "         # zsh `read` syntax
      #     echo
      #     if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
      #       git -C ${HOME}/.git-subrepo/ pull
      #     elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
      #       subrepo_upgrade
      #     fi
      #   else
      #     echo
      #     read -r -p "[git-subrepo upgrade] Do you want to upgrade git-subrepo? [Y/n]: " response   # bash `read` syntax
      #     echo
      #     if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
      #       git -C ${HOME}/.git-subrepo/ pull
      #     elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
      #       subrepo_upgrade
      #     fi
      #   fi
      # }
      # if [[ $(git -C ${HOME}/.git-subrepo/ remote show origin) =~ "out of date" ]]
      # then
      #   printf "\n  ${Red}git-subrepo is out of date with origin${Color_Off}\n"
      #   subrepo_upgrade
      # fi
    # Source the .rc file for git-subrepo
      source ${HOME}/.git-subrepo/.rc

# Logging
  ## Log all the things
    logall() {
        script -af "$l_logs_path"/Shell_Logs/typescript_$(date '+%F_%H-%M-%S-%N').txt
    }

  ## Make typescripts readable
    logprint() {
        echo "Printing out typescript(s) to file."
        shell_logs="$l_logs_path"/Shell_Logs
        mkdir -p $shell_logs/"originals"
        cp -aur $shell_logs/typescript_* $shell_logs/originals
        for i in $shell_logs/typescript_* ; do
          perl -pe 's/\e([^\[\]]|\[.*?[a-zA-Z]|\].*?\a)//g' "$i" | col -b > "$i.log"
        done
        rename $shell_logs/typescript_ $shell_logs/shell-log_ $shell_logs/typescript_*
        rm -r $shell_logs/shell-log_*.txt
        find $shell_logs/* -name '*.txt.log' -exec sh -c 'mv "$0" "${0%.txt.log}.log"' {} \;
    }

  ## Ask to log session
    logask() {
      if [[ "$(ps -ef | awk '$2==pid' pid=$$ | awk -F/ '{print $NF}')" == zsh ]] ; then   # robust check if shell is zsh as `read` syntax changes
        echo
        read "response?[logask] Do you want to log this shell session? [Y/n]: "         # zsh `read` syntax
        echo
        if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
          logall
        elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
          logask
        fi
      else
        echo
        read -r -p "[logask] Do you want to log this shell session? [Y/n]: " response   # bash `read` syntax
        echo
        if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
          logall
        elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
          logask
        fi
      fi
    }

  ## Auto logask
    if [[ "$(echo "${PWD}")" != "/home/${USER}" ]] ; then
      logask
    fi
