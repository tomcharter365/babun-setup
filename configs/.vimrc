" /home/${USER}/.vimrc
"""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""
" [Ultimate vimrc](https://github.com/amix/vimrc)
"""""""""""""""""""""""""""""""""""""

"" Awesome version

set runtimepath+=~/.vim_runtime
source ~/.vim_runtime/vimrcs/basic.vim
source ~/.vim_runtime/vimrcs/filetypes.vim
source ~/.vim_runtime/vimrcs/plugins_config.vim
source ~/.vim_runtime/vimrcs/extended.vim
try
  source ~/.vim_runtime/my_configs.vim
  catch
endtry

"""""""""""""""""""""""""""""""""""""
" Basic Behavior
"""""""""""""""""""""""""""""""""""""

set nocompatible
filetype off
" syntax on

set encoding=utf-8                              " Set encoding to UTF-8
scriptencoding utf-8                            " Specify scriptencoding to utf-8
setlocal spell spelllang=en_us                  " Enable spell check and set language to English
" set number                                      " Show line numbers
" set nowrap                                      " Disable line wrapping
set wrap                                        " Enable line wrapping
set lazyredraw                                  " Redraw screen only when we need to
set showmatch                                   " Highlight matching parentheses / brackets [{()}]
set clipboard=unnamed                           " Windows primary clipboard ([yy] to yank)
" set autoindent                                " Enable auto indentation
" set smartindent                               " Enable smart indentation
set wildmenu                                  " Visual autocomplete for command menu
set wildmode=list:longest,full                " First tab shows logest common command; Second tab shows all previous completions
" set ttyfast                                   " Sets term to xterm
" set backspace=indent,eol,start                " Fix weird backspace, delete and automatically-inserted indentation issues (Mac only)

"" Settings for the cursor
set visualbell                                  " Blink cursor on error, instead of beeping
set mouse=a                                     " Enable mouse support (might not work well on Mac OS X)
set scrolloff=10                                " Minimal number of screen lines to keep above and below the cursor
set cursorline                                  " Enable highlighting of the current line

"" Set vim files
set backupdir=./.backup,.,/tmp                  " Set directory to save backup files in
set undofile                                    " Keep undo files for cross session undo / redo
set swapfile                                    " Use a swapfile for the buffer
set directory=~/.local/tmp,/var/tmp,/tmp        " Set directory to save swap files in
" set dir=~/.local/tmp,/var/tmp,/tmp              " Set directory to save swap files in

"" Setings for tabs
set softtabstop=2                               " Number of softtabs a mix of spaces and tabs is used instead on just tabs
" set tabstop=2                                   " Number of spaces used in a tab
set shiftwidth=2                                " Number of spaces to use for each step of (auto)indent
set shiftround                                  " Round indent to multiple of 'shiftwidth'.  Applies to > and < commands
set smarttab                                    " A tab in front of a line inserts blanks according to 'shiftwidth', 'tabstop', or 'softtabstop'
set expandtab                                   " In Insert mode: Use the appropriate number of spaces to insert a tab. To insert a real tab when 'expandtab' is on, use CTRL-V<Tab>
set list listchars=tab:»·,trail:·               " See whitespace characters in tabs and spaces, also see trailinig whitespace but no eol chars

"""""""""""""""""""""""""""""""""""""
" Plugin Section
"""""""""""""""""""""""""""""""""""""

"" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

"" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"" Vundel Plugins
" Plugin 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
" Plugin 'junegunn/fzf.vim'                 " A command-line fuzzy finder
" Plugin 'jalvesaq/nvim-r'                  " Vim for R
" Plugin 'ervandew/supertab'                " Perform all your vim insert mode completions with Tab
Plugin 'ryanoasis/vim-devicons'           " Adds file type glyphs/icons to popular Vim plugins: NERDTree, vim-airline, Powerline, Unite, vim-startify and more
" Plugin 'derekwyatt/vim-scala'             " Vim for scala

"" Ultimate vimrc Plugins - already installed (sources_non_forked)
" Plugin 'mileszs/ack.vim'                  " Vim plugin for the Perl module / CLI script 'ack'
" Plugin 'w0rp/ale'                         " Asynchronous linting/fixing for Vim and Language Server Protocol (LSP) integration
" Plugin 'jiangmiao/auto-pairs'             " Vim plugin, insert or delete brackets, parens, quotes in pair
" Plugin 'yuttie/comfortable-motion.vim'    " Brings physics-based smooth scrolling to the Vim world!
" Plugin 'morhetz/gruvbox'                  " Retro groove color scheme for Vim
" Plugin 'itchyny/lightline.vim'            " A light and configurable statusline/tabline plugin for Vim
" Plugin 'maximbaz/lightline-ale'           " ALE indicator for the lightline vim plugin
" Plugin 'vim-scripts/mayansmoke'           " Pleasant and ergonomic light-background color scheme
" Plugin 'mru.vim'                          " Plugin to manage Most Recently Used (MRU) files
" Plugin 'scrooloose/nerdtree'              " A tree explorer plugin for vim
" Plugin 'nginx.vim'                        " highlights configuration files for nginx, the high-performance web server
" Plugin 'godlygeek/tabular'                " Vim script for text filtering and alignment
" Plugin 'tomtom/tlib_vim'                  " Some utility functions for VIM
" Plugin 'tpope/vim-abolish'                " easily search for, substitute, and abbreviate multiple variants of a word
" Plugin 'marcweber/vim-addon-mw-utils'     " interpret a file by function and cache file automatically
" Plugin 'sophacles/vim-bundle-mako'        " A collection of vim scripts for the mako templating engine, in a vim bundle form
" Plugin 'kchmck/vim-coffee-script'         " CoffeeScript support for vim
" Plugin 'altercation/vim-colors-solarized' " precision colorscheme for the vim text editor
" Plugin 'tpope/vim-commentary'             " comment stuff out
" Plugin 'terryma/vim-expand-region'        " Vim plugin that allows you to visually select increasingly larger regions of text using the same key combination
" Plugin 'tpope/vim-fugitive'               " A Git wrapper so awesome, it should be illegal
" Plugin 'airblade/vim-gitgutter'           " A Vim plugin which shows a git diff in the gutter (sign column) and stages/undoes hunks
" Plugin 'haesken/vim-go'                   " Go development plugin for Vim
" Plugin 'lunaru/vim-less'                  " LessCSS Syntax support in Vim
" Plugin 'gabrielelana/vim-markdown'        " Markdown for Vim: a complete environment to create Markdown files with a syntax highlight that doesn't suck
" Plugin 'terryma/vim-multiple-cursors'     " True Sublime Text style multiple selections for Vim
" Plugin 'therubymug/vim-pyte'              " Henning Hasemann's pyte vim theme packaged to work with Tim Pope's pathogen plugin
" Plugin 'tpope/vim-repeat'                 " enable repeating supported plugin maps with "."
" Plugin 'kana/vim-surround'                " Operators to edit surrounding text
" Plugin 'maxbrunsfeld/vim-yankstack'       " A lightweight implementation of emacs's kill-ring for vim
" Plugin 'amix/vim-zenroom2'                " A Vim extension that emulates iA Writer environment when editing Markdown, reStructuredText or text files

"" Ultimate vimrc Plugins - already installed (sources_forked)
" amix/vimrc/sources_forked/peaksea/colors/peaksea.vim
" amix/vimrc/sources_forked/set_tabline/plugin/set_tabline.vim
" amix/vimrc/sources_forked/vim-irblack-forked/colors/ir_black.vim
" amix/vimrc/sources_forked/vim-peepopen/plugin/peepopen.vim

" All of your Plugins must be added before the following line
call vundle#end()                         " required
filetype plugin indent on                 " required

"""""""""""""""""""""""""""""""""""""
" Plugin Configuration
"""""""""""""""""""""""""""""""""""""
"" fzf
" nnoremap <C-f> :FZF<cr>                 " fuzzy search ctrl+f

"" NERDtree
map <C-n> :NERDTreeToggle<CR>           " ctrl+n open file view
let g:NERDTreeWinPos = "left"           " Alawys show on left

"" supertab
" let g:SuperTabDefaultCompletionType = "<C-X><C-O>"

"" vim-go
let g:go_version_warning = 0            " disable vim version warnings
let g:go_fmt_command = "goimports"      " use goimports instead of gofmt
let g:go_fmt_autosave = 0               " disable fmt on save
let g:go_highlight_types = 1            " Highlight types
let g:go_highlight_fields = 1           " Highlight fields
let g:go_highlight_functions = 1        " Highlight function and method names
let g:go_highlight_function_calls = 1   " Highlight method invocations
let g:go_highlight_operators = 1        " Highlight operators
let g:go_highlight_extra_types = 1      " Highlight extra types

"""""""""""""""""""""""""""""""""""""
" Shortcuts
"""""""""""""""""""""""""""""""""""""
set pastetoggle=<F2>        " paste mode  [f2]
nnoremap <C-t> :tabe<cr>    " new tab     [ctrl+t]
